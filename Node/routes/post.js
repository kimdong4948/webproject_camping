var express  = require('express');
var router = express.Router();
var commentModel = require('../models/mongooseModels/commentModel');

// Index
router.get('/:name', function(req, res){
  commentModel.find({})
    .populate('user_id')

    .exec(function(err, post){
      if(err) return res.json(err);
      res.render('post/index', {
        post:post,
      });
    });
});





module.exports = router;
