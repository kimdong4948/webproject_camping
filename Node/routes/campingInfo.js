var express = require('express');
var router = express.Router();

var controller = require( '../controllers/campingInfoController' );

/* GET users listing. */


router.get('/detail', controller.getDetailInfo);

router.post('/campSpotAllcount' , controller.allSpotCount);

router.post('/campInfoList' , controller.campInfoList);

router.post('/imageList',  controller.imageList);

//상세정보
router.post('/getDetailCampingInfo', controller.getDetailCampingInfo);

router.post('/chngLocation', controller.chngLocation);

router.get('/', controller.getListInfo );



module.exports = router;
