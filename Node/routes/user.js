var express = require('express');
var router = express.Router();
var userModel = require('../models/mongooseModels/userModel');


//myPage
router.get('/:name', function(req, res){
  userModel.findOne({name:req.params.name}, function(err, user){
    if(err) return res.json(err);
    res.render('user/myPage', {user:user});
  });
});

// destroy
router.delete('/:name', function(req, res){
  userModel.deleteOne({email:req.params.email}, function(err){
    if(err) return res.json(err);
    res.redirect('/campingInfo');
  });
});


module.exports = router;



