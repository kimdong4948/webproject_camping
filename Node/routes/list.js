var express = require('express');
var router = express.Router();

var controller = require('../controllers/listController');

/* GET users listing. */
router.get('/',  controller.getInfo );

module.exports = router;
