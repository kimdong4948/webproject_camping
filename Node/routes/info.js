var express = require('express');
var router = express.Router();
var passport = require('passport');

var controller = require('../controllers/campingInfoController');

/* GET users listing. */
router.get('/', function(req, res, next) {

    res.render('info', { user: req.user });

});

router.post('/',  controller.basedList);

router.post('/basedList',  controller.basedList);

router.post('/WriteXml', controller.getXmlInfo);

router.post('/locationBasedList',  controller.locationBasedList);
router.post('/searchList',  controller.searchList);
router.post('/imageList',  controller.imageList);


router.post('/areaCode',  controller.areaCode);




module.exports = router;
