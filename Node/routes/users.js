var express = require('express');
var router = express.Router();
var User = require('../models/User');
var controller = require( '../controllers/userController' );


//home
router.get('/', function(req, res){
  //유저정보 세션에 저장
  req.session.userSession = JSON.stringify(req.user);
  res.write("<script>alert('SignUp Successful!')</script>");
  res.write("<script>window.location=\"/login\"</script>");
});

// create
router.post('/', function(req, res){
  User.create(req.body, function(err, user){
    if(err) return res.json(err);
    res.redirect('/users');
  });
});

// New
router.get('/new', function(req, res){
  res.render('users/new');
});


//myPage
router.get('/:username', function(req, res){
  User.findOne({username:req.params.username}, function(err, user){
    if(err) return res.json(err);
    res.render('users/myPage', {user:user});
  });
});


// edit
router.get('/:username/edit', function(req, res){
  User.findOne({username:req.params.username}, function(err, user){
    if(err) return res.json(err);
    res.render('users/edit', {user:user});
  });
});

// update
router.put('/:username', function(req, res, next){
  User.findOne({username:req.params.username})
    .select('password')
    .exec(function(err, user){
      if(err) return res.json(err);

      // update
      user.originalPassword = user.password;
      user.password = req.body.newPassword? req.body.newPassword : user.password;
      for(var p in req.body){
        user[p] = req.body[p];
      }

      // save updated user
      user.save(function(err, user){
        if(err) return res.json(err);
        res.redirect('/users/'+user.username);
      });
  });
});

// destroy
router.delete('/:username', function(req, res){
  User.deleteOne({username:req.params.username}, function(err){
    if(err) return res.json(err);
    res.redirect('/campingInfo');
  });
});

router.get('/find/:id', controller.findUser );

router.get('/create/:id', controller.createUser);

module.exports = router;



