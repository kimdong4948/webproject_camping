var express = require('express');
var router = express.Router();

var commentController = require( '../controllers/commentController' );


router.post('/getCommentList', commentController.getCommentList);

router.post('/saveComment', commentController.saveComment);

router.post('/deleteComment', commentController.deleteComment);

router.post('/getAvgRating', commentController.getAvgCommentRating);

module.exports = router;
