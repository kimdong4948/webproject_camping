'use strict';

const express = require('express');
const passport = require('passport');
const controller = require('../controllers/loginController');

const asyncHandler = require('express-async-handler');
const userModel = require('../models/mongooseModels/userModel');

const router = express.Router();



/* GET users listing. */
//router.get('/', controller.loginForm );

// Login 이동하도록 함
router.get('/', function (req,res) {
  var username = req.flash('username')[0];
  var errors = req.flash('errors')[0] || {};
  res.render('home/login', {
    username:username,
    errors:errors
  });
});


router.get('/logOut', controller.logOut );





//예슬씨 local login  passport.js 로 이동함
router.post('/', 
  function(req,res,next){
    var errors = {};
    var isValid = true;

    if(!req.body.username){
      isValid = false;
      errors.username = 'Username is required!';
    }
    if(!req.body.password){
      isValid = false;
      errors.password = 'Password is required!';
    }

    if(isValid){
      next();
    }
    else {
      req.flash('errors',errors);
      res.redirect('/login');
    }
  },passport.authenticate('local', {
    successRedirect : '/campingInfo',
    failureRedirect : '/login'
  }), 
    (req,res) => {
        //req.flash('info', 'Flash is back!')     
        req.session.userSession = JSON.stringify(req.user);                                         
        res.redirect('/campingInfo');

    }
);

//router.post('/',asyncHandler(insert));


router.get('/kakao',passport.authenticate('kakao', {
      failureRedirect: '/login',
    })
    //,users.signin
  )
  
router.get('/oauth',passport.authenticate('kakao', {
    failureRedirect: '/login',
})

,(req,res) => {
    //req.flash('info', 'Flash is back!')                                              

    //유저정보 세션에 저장
    req.session.userSession = JSON.stringify(req.user);
    //console.log(req.session.userSession);
    res.redirect('/campingInfo');

}
//,users.authCallback
)







module.exports = router;

