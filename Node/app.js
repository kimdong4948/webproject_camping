var createError = require('http-errors');
var express = require('express');
var path = require('path');

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var errorHandler = require('errorhandler');
var expressErrorHandler = require('express-error-handler');

var logger = require('morgan');
var dotenv = require('dotenv');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

//var passport = require('./api/passport');

require('./src/api/mongoConn');



//세션에 대한 설정
const session = require('express-session');
const passport = require('passport');
//var passport = require('./config/passport');
const flash = require('connect-flash');





var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var listRouter = require('./routes/list');
var campingInfoRouter = require('./routes/campingInfo');

var infoRouter = require('./routes/info');

var commentRouter = require('./routes/comment');

var loginRouter = require('./routes/login');

var noticeRouter = require('./routes/notice');


require('./src/api/passport');


var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// dev , short , common , combined
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

//jqx import
app.use('/jqx', express.static(path.join(__dirname,"./node_modules/jqwidgets-framework")));
//pagination
app.use('/pagination', express.static(path.join(__dirname,"./node_modules/paginationjs/dist")));
app.use('/twbs-pagination', express.static(path.join(__dirname,"./node_modules/twbs-pagination")));

app.use(cookieParser());

app.use(session({secret:'MySecret', resave:true, saveUninitialized:true}));

app.use(methodOverride('_method'));
app.use(express.static(__dirname+'/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(flash());
app.use(passport.initialize()); // passport 구동
app.use(passport.session()); // 세션 연결



//어느곳이든 쓸수 있도록 정보를 계속 넘김
app.use(function(req,res,next){

  res.locals.isAuthenticated = req.isAuthenticated();
  res.locals.currentUser = req.user;
  
  next();
});





app.use('/',  indexRouter);
app.use('/users', usersRouter);
app.use('/list', listRouter);

app.use('/campingInfo', campingInfoRouter);
//session 정보가 필요한 화면인경우 넣어주도록 한다
app.use('/info', infoRouter);

app.use('/comment', commentRouter);

app.use('/login', loginRouter);

app.use('/notice',noticeRouter);

app.use('/users', require('./routes/users'));
app.use('/', require('./routes/home'));
app.use('/user', require('./routes/user'));
app.use('/post', require('./routes/post'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(res.render('common/404'));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
