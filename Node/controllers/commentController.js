//리뷰에 대한 코멘트 Controller


// 2021 06 17 init - dhkim

//comment Model
const Comment = require('../models/mongooseModels/commentModel');


//서울 시간 format 으로 변경
// const moment = require('moment');
// moment.tz.setDefault("Asia/Seoul");

// //tiem stamp 변경
// let date = moment().format('YYYY-MM-DD HH:mm:ss');

//리스트 가져오기
exports.getCommentList = (req,res,next) => {
    //join user collection
    Comment.find({content_id: req.body.contentId}).populate('user_id').sort({ created_dt : -1}).exec()
    .then( result => {

        res.json( {status: "S" ,  message : "success" , data : result });
    })
    .catch(err => {
        console.log(err);
        res.json({status: "E" ,  message : err});
    });

}

//코멘트 저장
exports.saveComment = (req,res,next) => {
  
    if(typeof req.body.rating != 'Number'){
        req.body.rating = Number(req.body.rating);
    }

    if(typeof req.body.contentId != 'Number'){
        req.body.contentId = Number(req.body.contentId);
    }

    //console.log(req.session.passport.user);
    
    if(req.session.passport.user.provider == null || req.session.passport.user.provider == 'undefined'){
        comment = new Comment({
            user_id: req.session.passport.user.id,
            //user_profile_url: JSON.parse(req.session.userSession).kakao.kakao_account.profile.thumbnail_image_url,
            content_id: req.body.contentId,
            rating: req.body.rating,
            comment: req.body.comment,
        });
    }else{ //소셜로그인 판단
        comment = new Comment({
            user_id: JSON.parse(req.session.userSession)._id,
            user_profile_url: JSON.parse(req.session.userSession).kakao.kakao_account.profile.thumbnail_image_url,
            content_id: req.body.contentId,
            rating: req.body.rating,
            comment: req.body.comment,
        });
    }
    
    

    comment.save()
    .then( result => {
        res.json( {status: "S" ,  message : "success" , data : result });
    })
    .catch(err => {
        console.log(err);
        res.json({status: "E" ,  message : err});
    });

}
  
//코멘트삭제
exports.deleteComment = (req,res,next) => {
    
    if(typeof req.body.commentSeq != 'Number'){
        req.body.commentSeq = Number(req.body.commentSeq);
    }

    Comment.deleteOne({comment_seq:req.body.commentSeq})
    .then( result => {
        res.json( {status: "S" ,  message : "success" , data : result });
    })
    .catch(err => {
        console.log(err);
        res.json({status: "E" ,  message : err});
    });

}



//게시물에 평균 리부 점수 구하기
exports.getAvgCommentRating = (req,res,next) => {
    console.log("test" + req.body.contentId);
    if(typeof req.body.contentId != 'Number'){
        req.body.contentId = Number(req.body.contentId);
    }


    Comment.aggregate([
        {   $match : { content_id : req.body.contentId }},
        {
            $group : {
                _id : "$content_id" ,
                averageRating: { $avg: "$rating" }
            }
        }
    ])
    .then( result => {
        res.json( {status: "S" ,  message : "success" , data : result });
    })
    .catch(err => {
        console.log(err);
        res.json({status: "E" ,  message : err});
    });
}