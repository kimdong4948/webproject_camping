// 에러코드
// 00	NORMAL_CODE	정상
// 01	APPLICATION_ERROR	어플리케이션 에러
// 02	DB_ERROR	데이터베이스 에러
// 03	NODATA_ERROR	데이터없음 에러
// 04	HTTP_ERROR	HTTP 에러
// 05	SERVICETIMEOUT_ERROR	서비스 연결실패 에러
// 10	INVALID_REQUEST_PARAMETER_ERROR	잘못된 요청 파라메터 에러
// 11	NO_MANDATORY_REQUEST_PARAMETERS_ERROR	필수요청 파라메터가 없음
// 12	NO_OPENAPI_SERVICE_ERROR	해당 오픈API서비스가 없거나 폐기됨
// 20	SERVICE_ACCESS_DENIED_ERROR	서비스 접근거부
// 21	TEMPORARILY_DISABLE_THE_SERVICEKEY_ERROR	일시적으로 사용할 수 없는 서비스 키
// 22	LIMITED_NUMBER_OF_SERVICE_REQUESTS_EXCEEDS_ERROR	서비스 요청제한횟수 초과에러
// 30	SERVICE_KEY_IS_NOT_REGISTERED_ERROR	등록되지 않은 서비스키
// 31	DEADLINE_HAS_EXPIRED_ERROR	기한만료된 서비스키
// 32	UNREGISTERED_IP_ERROR	등록되지 않은 IP
// 33	UNSIGNED_CALL_ERROR	서명되지 않은 호출
// 99	UNKNOWN_ERROR	기타에러




//const Camp = require('../models/--');

const request = require('request');
const convert = require('xml-js');
const sessionstorage = require('sessionstorage');

const fs = require('fs');


//camping Model
const Camp = require('../models/mongooseModels/campingModel');




//고캠핑 정보
var campingUrl = process.env.goCampingUri;

//한국 관광공사 정보
var koreaTourUrl = process.env.koreaTourUri;



//XML로 정보 다운받기위한 함수 특별한경우 사용
var WriteFileXml = (type ,param ,targetApi, callback ) => {

    var vparam ,vurl= null;

    if(type == 'koreaTour'){
        vurl = koreaTourUrl + targetApi;
    }else{
        vurl = campingUrl + targetApi;
    }
    
    vparam = '?' + encodeURIComponent('ServiceKey') + '=' + encodeURIComponent(process.env.ApiKey); /* Service Key*/
    vparam += '&' + encodeURIComponent('MobileOS') + '=' + encodeURIComponent(process.env.ApiOs); /* ETC,IOS (아이폰), AND (안드로이드), WIN (윈도우폰) */
    vparam += '&' + encodeURIComponent('MobileApp') + '=' + encodeURIComponent(process.env.ApiApp); /* 서비스명=어플명 */

    vparam += param;
    request({
        url: vurl + vparam,
        method: 'GET'
    }, function (error, response, body) {
        //var result = '';

        console.log('Status', response.statusCode);
        console.log('Headers', JSON.stringify(response.headers));

        var xmlToJson = convert.xml2json(body, {compact: false, spaces: 4});


        fs.writeFile('campingInfo.xml', body, function(err){ 
            if (err === null) { 
                console.log('success'); 
                //result += xmlToJson;
                callback({CODE:"S" , MESSAGE: "Success"});

            } else {
                callback({CODE:"E" , MESSAGE: "fail"});
            } 
            
        });

        
    });

};


//캠핑정보 API CALL 하는 공통 함수
var callAPI = (type ,param ,targetApi, callback ) =>{
    var vparam ,vurl= null;

		if(type == 'koreaTour'){
			vurl = koreaTourUrl + targetApi;
		}else{
			vurl = campingUrl + targetApi;
		}
		
    vparam = '?' + encodeURIComponent('ServiceKey') + '=' + encodeURIComponent(process.env.ApiKey); /* Service Key*/
    vparam += '&' + encodeURIComponent('MobileOS') + '=' + encodeURIComponent(process.env.ApiOs); /* ETC,IOS (아이폰), AND (안드로이드), WIN (윈도우폰) */
    vparam += '&' + encodeURIComponent('MobileApp') + '=' + encodeURIComponent(process.env.ApiApp); /* 서비스명=어플명 */

    vparam += param;
    
 
    request({
        url: vurl + vparam,
        method: 'GET',
    }, function (error, response, body) {
        console.log('Status', response.statusCode);
        console.log('Headers', JSON.stringify(response.headers));

        //xml 형식이므로 convert 필요
        var xmlToJson = convert.xml2json(body, {compact: true, spaces: 4});

        callback(JSON.parse(xmlToJson).response.body);
    });
}

//미사용
var callAPIArray =  async (type ,params ,targetApi, callback ) =>{
    var vparam ,vurl= null;

		if(type == 'koreaTour'){
			vurl = koreaTourUrl + targetApi;
		}else{
			vurl = campingUrl + targetApi;
		}
		

    if(params.length <= 1){
        vparam = '?' + encodeURIComponent('ServiceKey') + '=' + encodeURIComponent(process.env.ApiKey); /* Service Key*/
        vparam += '&' + encodeURIComponent('MobileOS') + '=' + encodeURIComponent(process.env.ApiOs); /* ETC,IOS (아이폰), AND (안드로이드), WIN (윈도우폰) */
        vparam += '&' + encodeURIComponent('MobileApp') + '=' + encodeURIComponent(process.env.ApiApp); /* 서비스명=어플명 */
        vparam += params;

        await request({
            url: vurl + vparam,
            method: 'GET',
        }, function (error, response, body) {
            console.log('Status', response.statusCode);
            console.log('Headers', JSON.stringify(response.headers));
    
            //xml 형식이므로 convert 필요
            var xmlToJson = convert.xml2json(body, {compact: true, spaces: 4});
    
            callback(JSON.parse(xmlToJson).response.body);
        });

    }else{
        var jArray = new Array();
        var Count = 1;

        for(var i=0; i<params.length; i++){
            vparam = '?' + encodeURIComponent('ServiceKey') + '=' + encodeURIComponent(process.env.ApiKey); /* Service Key*/
            vparam += '&' + encodeURIComponent('MobileOS') + '=' + encodeURIComponent(process.env.ApiOs); /* ETC,IOS (아이폰), AND (안드로이드), WIN (윈도우폰) */
            vparam += '&' + encodeURIComponent('MobileApp') + '=' + encodeURIComponent(process.env.ApiApp); /* 서비스명=어플명 */
            vparam += params[i];
            console.log('params : ' + params[i]);
            
            await request({
                url: vurl + vparam,
                method: 'GET',
            }, function (error, response, body) {
                console.log('Status', response.statusCode);
                console.log('Headers', JSON.stringify(response.headers));
        
                //xml 형식이므로 convert 필요
                var xmlToJson = convert.xml2json(body, {compact: true, spaces: 4});
                console.log(JSON.parse(xmlToJson).response.body);
                jArray.push(JSON.parse(xmlToJson).response.body.items);
                //callback(JSON.parse(xmlToJson).response.body);

                if(Count != params.length){
                    Count++;    
                }else{
                    //function call  
                    callback({items:jArray});
                }

                //특정 건수의 request가 다끝났을경우
            });

        }

    }

}





exports.getXmlInfo = (req,res,next) => {

    var queryParams = '';

    queryParams += '&' + encodeURIComponent('pageNo') + '=' + encodeURIComponent(req.body.pageNo); /* 	현재 페이지번호 */
    queryParams += '&' + encodeURIComponent('numOfRows') + '=' + encodeURIComponent(req.body.numOfRows); /* 	한 페이지 결과 수*/

    WriteFileXml('camping',queryParams , "/basedList" , (result) => {
        res.send(result); 
    });
}



//campingInfo 페이지 렌더 
//차후 변경 될 수 있음
exports.getListInfo = (req,res,next) => {
    res.render('campingInfo');
}

//⦁	응답 메시지 (Response Message) – XML,Json에서 “item”노드부터는 알파벳 순서로 정렬됨
// numOfRows	한 페이지 결과 수	4	0	10	한 페이지 결과 수
// pageNo	   페이지 번호		 옵션	현재 페이지 번호
// MobileOS	OS              필수            ETC,IOS (아이폰), AND (안드로이드), WIN (윈도우폰), ETC
// MobileApp	서비스명	필수	         AppTest	서비스명=어플명
// ServiceKey	인증키 (서비스키)	필수	인증키
// <?xml version="1.0" encoding="UTF-8" standalone="true"?>
// -<response>
// -<header>
// <resultCode>0000</resultCode>
// <resultMsg>OK</resultMsg>
// </header>
// -<body>
// -<items>
// -<item>
// <addr1>경상북도 칠곡군 가산면 응추리 </addr1>
// <addr2>183 </addr2>
// <allar>28000</allar>
// <autoSiteCo>48</autoSiteCo>
// <bizrno>403-39-61259</bizrno>
// <caravAcmpnyAt>N</caravAcmpnyAt>
// <caravSiteCo>0</caravSiteCo>
// <clturEventAt>N</clturEventAt>
// <contentId>3429</contentId>
// <createdtime>2020-06-04 14:37:01 </createdtime>
// <doNm>경상북도</doNm>
// <exprnProgrmAt>N</exprnProgrmAt>
// <facltDivNm>민간</facltDivNm>
// <facltNm>한티별빛아래관광농원 야영장 </facltNm>
// <featureNm>카페</featureNm>
// <glampSiteCo>0</glampSiteCo>
// <gnrlSiteCo>0</gnrlSiteCo>
// <homepage>http://cafe.daum.net/hantistar-camp</homepage>
// <induty>일반야영장</induty>
// <indvdlCaravSiteCo>0</indvdlCaravSiteCo>
// <insrncAt>N</insrncAt>
// <intro>한티 별빛아래 오토캠핑장은 경북 칠곡군 가산면에 자리 잡았다. 칠곡군청을 기점으로 33㎞가량 떨어졌으며, 자동차에 몸을 싣고 호국로, 남원로, 79번 지방도를 차례로 달리면 닿는다. 도착까지 걸리는 시간은 50분 안팎이다. 이곳은 ‘높은 고개’란 의미를 지닌 한티재 인근에 위치했다. 고지대인 덕택에 한여름 낮 시간에는 다른 지역에 비해 무덥지 않다. 해가 진 저녁에는 닭살이 살짝 돋는 선선함을 느낄 정도다. 게다가 캠핑장에서 발 아래로 내려다보는 풍경은 감탄사를 자아낼 정도로 멋지다. 캠핑장에는 48면의 자동차 야영장을 마련했다. 바닥 형태는 파쇄석이며, 사이트 크기는 가로 6m 세로 9m다. 개인 트레일러는 물론 카라반 동반 입장이 가능하다. 주말 운영을 원칙으로 사계절 내내 문을 연다. 캠핑장 인근에 치산관광지, 팔공산도립공원, 블루데이식물원 등이 있어 연계관광이 용이하다. 팔공산 주변에 여러 음식점도 성업 중이라 먹을거리를 찾아 나서기에 부담이 없다.</intro>
// <lctCl>산</lctCl>
// <lineIntro>발 아래로 내려다보는 풍경이 일품</lineIntro>
// <manageNmpr>2</manageNmpr>
// <manageSttus>운영</manageSttus>
// <mangeDivNm>직영</mangeDivNm>
// <mapX>128.6142847</mapX>
// <mapY>36.0345423</mapY>
// <modifiedtime>2020-06-04 14:37:01 </modifiedtime>
// <prmisnDe>2015-08-11</prmisnDe>
// <resveCl>온라인실시간예약</resveCl>
// <resveUrl>http://r6.camperstory.com/?1789</resveUrl>
// <sigunguNm>칠곡군</sigunguNm>
// <siteBottomCl1>0</siteBottomCl1>
// <siteBottomCl2>48</siteBottomCl2>
// <siteBottomCl3>0</siteBottomCl3>
// <siteBottomCl4>0</siteBottomCl4>
// <siteBottomCl5>0</siteBottomCl5>
// <siteMg1Co>48</siteMg1Co>
// <siteMg1Vrticl>9</siteMg1Vrticl>
// <siteMg1Width>6</siteMg1Width>
// <siteMg2Co>0</siteMg2Co>
// <siteMg2Vrticl>0</siteMg2Vrticl>
// <siteMg2Width>0</siteMg2Width>
// <siteMg3Co>0</siteMg3Co>
// <siteMg3Vrticl>0</siteMg3Vrticl>
// <siteMg3Width>0</siteMg3Width>
// <sitedStnc>3</sitedStnc>
// <tel>054-972-8032</tel>
// <trlerAcmpnyAt>N</trlerAcmpnyAt>
// <wtrplCo>0</wtrplCo>
// <zipcode>718911</zipcode>
// </item>
// </items>
// <numOfRows>10</numOfRows>
// <pageNo>1</pageNo>
// <totalCount>2390</totalCount>
// </body>
// </response>

// 기본정보 목록을 조회하는 기능입니다.
exports.basedList = (req,res,next) => {

    var queryParams = '';

    queryParams += '&' + encodeURIComponent('pageNo') + '=' + encodeURIComponent(req.body.pageNo); /* 	현재 페이지번호 */
    queryParams += '&' + encodeURIComponent('numOfRows') + '=' + encodeURIComponent(req.body.numOfRows); /* 	한 페이지 결과 수*/
    console.log(queryParams);
    
    callAPI('camping',queryParams , "/basedList" , (result) => {
        res.json(result); 
    });
    
}


//⦁	응답 메시지 (Response Message) – XML,Json에서 “item”노드부터는 알파벳 순서로 정렬됨
// numOfRows	한 페이지 결과 수	4	0	10	한 페이지 결과 수
// pageNo	   페이지 번호		 옵션	현재 페이지 번호
// MobileOS	OS              필수            ETC,IOS (아이폰), AND (안드로이드), WIN (윈도우폰), ETC
// MobileApp	서비스명	필수	         AppTest	서비스명=어플명
// ServiceKey	인증키 (서비스키)	필수	인증키
//mapX	경도	필수	128.6142847	GPS X좌표(WGS84 경도 좌표)
//mapY	위도	필수	36.0345423	GPS Y좌표(WGS84 위도 좌표)
//radius	거리 반경	필수	2000	거리 반경(단위:m) , Max값 20000m=20Km
// <?xml version="1.0" encoding="UTF-8" standalone="true"?>
// -<response>
// -<header>
// <resultCode>0000</resultCode>
// <resultMsg>OK</resultMsg>
// </header>
// -<body>
// -<items>
// -<item>
// <addr1>경상북도 칠곡군 가산면 응추리 </addr1>
// <addr2>183 </addr2>
// <allar>28000</allar>
// <autoSiteCo>48</autoSiteCo>
// <bizrno>403-39-61259</bizrno>
// <caravAcmpnyAt>N</caravAcmpnyAt>
// <caravSiteCo>0</caravSiteCo>
// <clturEventAt>N</clturEventAt>
// <contentId>3429</contentId>
// <createdtime>2020-06-04 14:37:01 </createdtime>
// <doNm>경상북도</doNm>
// <exprnProgrmAt>N</exprnProgrmAt>
// <facltDivNm>민간</facltDivNm>
// <facltNm>한티별빛아래관광농원 야영장 </facltNm>
// <featureNm>카페</featureNm>
// <glampSiteCo>0</glampSiteCo>
// <gnrlSiteCo>0</gnrlSiteCo>
// <homepage>http://cafe.daum.net/hantistar-camp</homepage>
// <induty>일반야영장</induty>
// <indvdlCaravSiteCo>0</indvdlCaravSiteCo>
// <insrncAt>N</insrncAt>
// <intro>한티 별빛아래 오토캠핑장은 경북 칠곡군 가산면에 자리 잡았다. 칠곡군청을 기점으로 33㎞가량 떨어졌으며, 자동차에 몸을 싣고 호국로, 남원로, 79번 지방도를 차례로 달리면 닿는다. 도착까지 걸리는 시간은 50분 안팎이다. 이곳은 ‘높은 고개’란 의미를 지닌 한티재 인근에 위치했다. 고지대인 덕택에 한여름 낮 시간에는 다른 지역에 비해 무덥지 않다. 해가 진 저녁에는 닭살이 살짝 돋는 선선함을 느낄 정도다. 게다가 캠핑장에서 발 아래로 내려다보는 풍경은 감탄사를 자아낼 정도로 멋지다. 캠핑장에는 48면의 자동차 야영장을 마련했다. 바닥 형태는 파쇄석이며, 사이트 크기는 가로 6m 세로 9m다. 개인 트레일러는 물론 카라반 동반 입장이 가능하다. 주말 운영을 원칙으로 사계절 내내 문을 연다. 캠핑장 인근에 치산관광지, 팔공산도립공원, 블루데이식물원 등이 있어 연계관광이 용이하다. 팔공산 주변에 여러 음식점도 성업 중이라 먹을거리를 찾아 나서기에 부담이 없다.</intro>
// <lctCl>산</lctCl>
// <lineIntro>발 아래로 내려다보는 풍경이 일품</lineIntro>
// <manageNmpr>2</manageNmpr>
// <manageSttus>운영</manageSttus>
// <mangeDivNm>직영</mangeDivNm>
// <mapX>128.6142847</mapX>
// <mapY>36.0345423</mapY>
// <modifiedtime>2020-06-04 14:37:01 </modifiedtime>
// <prmisnDe>2015-08-11</prmisnDe>
// <resveCl>온라인실시간예약</resveCl>
// <resveUrl>http://r6.camperstory.com/?1789</resveUrl>
// <sigunguNm>칠곡군</sigunguNm>
// <siteBottomCl1>0</siteBottomCl1>
// <siteBottomCl2>48</siteBottomCl2>
// <siteBottomCl3>0</siteBottomCl3>
// <siteBottomCl4>0</siteBottomCl4>
// <siteBottomCl5>0</siteBottomCl5>
// <siteMg1Co>48</siteMg1Co>
// <siteMg1Vrticl>9</siteMg1Vrticl>
// <siteMg1Width>6</siteMg1Width>
// <siteMg2Co>0</siteMg2Co>
// <siteMg2Vrticl>0</siteMg2Vrticl>
// <siteMg2Width>0</siteMg2Width>
// <siteMg3Co>0</siteMg3Co>
// <siteMg3Vrticl>0</siteMg3Vrticl>
// <siteMg3Width>0</siteMg3Width>
// <sitedStnc>3</sitedStnc>
// <tel>054-972-8032</tel>
// <trlerAcmpnyAt>N</trlerAcmpnyAt>
// <wtrplCo>0</wtrplCo>
// <zipcode>718911</zipcode>
// </item>
// </items>
// <numOfRows>10</numOfRows>
// <pageNo>1</pageNo>
// <totalCount>3</totalCount>
// </body>
// </response>

//내주변 좌표를 기반으로 목록을 조회하는 기능입니다.
exports.locationBasedList = (req,res,next) => {

    var queryParams = '';

    queryParams += '&' + encodeURIComponent('pageNo') + '=' + encodeURIComponent(req.body.pageNo); /* 	현재 페이지번호  */
    queryParams += '&' + encodeURIComponent('numOfRows') + '=' + encodeURIComponent(req.body.numOfRows); /* 	한 페이지 결과 수  */
    queryParams += '&' + encodeURIComponent('mapX') + '=' + encodeURIComponent(req.body.mapX); /* 	경도 */
    queryParams += '&' + encodeURIComponent('mapY') + '=' + encodeURIComponent(req.body.mapY); /* 	위도  */
    queryParams += '&' + encodeURIComponent('radius') + '=' + encodeURIComponent(req.body.radius); /* 	거리반경  */    
    
    callAPI('camping',queryParams , "/locationBasedList" , (result) => {
        res.json(result); 
    });
}



//⦁	응답 메시지 (Response Message) – XML,Json에서 “item”노드부터는 알파벳 순서로 정렬됨
// numOfRows	한 페이지 결과 수	4	0	10	한 페이지 결과 수
// pageNo	   페이지 번호		 옵션	현재 페이지 번호
// MobileOS	OS              필수            ETC,IOS (아이폰), AND (안드로이드), WIN (윈도우폰), ETC
// MobileApp	서비스명	필수	         AppTest	서비스명=어플명
// ServiceKey	인증키 (서비스키)	필수	인증키
// keyword	요청 키워드	 필수 	ex) 야영장(%EC%95%BC%EC%98%81%EC%9E%A5)	검색 요청할 키워드 (인코딩 필요)



//키워드로 검색을 하여 목록을 조회하는 기능입니다.
exports.searchList = (req,res,next) => {

    var queryParams='';

    queryParams += '&' + encodeURIComponent('pageNo') + '=' + encodeURIComponent(req.body.pageNo); /* 	현재 페이지번호  */
    queryParams += '&' + encodeURIComponent('numOfRows') + '=' + encodeURIComponent(req.body.numOfRows); /* 	한 페이지 결과 수  */
    queryParams += '&' + encodeURIComponent('keyword') + '=' + encodeURIComponent(req.body.keyword); /* 	야영장(%EC%95%BC%EC%98%81%EC%9E%A5)	검색 요청할 키워드 (인코딩 필요)  */

    callAPI('camping',queryParams , "/searchList" , (result) => {
        res.json(result); 
    });
}




//⦁	응답 메시지 (Response Message) – XML,Json에서 “item”노드부터는 알파벳 순서로 정렬됨
// numOfRows	한 페이지 결과 수	4	0	10	한 페이지 결과 수
// pageNo	   페이지 번호		 옵션	현재 페이지 번호
// MobileOS	OS              필수            ETC,IOS (아이폰), AND (안드로이드), WIN (윈도우폰), ETC
// MobileApp	서비스명	필수	         AppTest	서비스명=어플명
// ServiceKey	인증키 (서비스키)	필수	인증키
// contentId	콘텐츠ID 필수	3429	콘텐츠ID




//각 컨텐츠에 해당하는 이미지URL 목록을 조회하는 기능입니다.
exports.imageList = (req,res,next) => {

    var queryParams='';

    queryParams += '&' + encodeURIComponent('contentId') + '=' + encodeURIComponent(req.body.contentId); /* 	콘텐츠ID  */
		console.log(queryParams);
    callAPI('camping',queryParams , "/imageList" , (result) => {
        res.json(result.items.item); 
    });
}





//numOfRows	한 페이지 결과 수 옵션	한 페이지 결과 수
//ageNo	페이지 번호	옵션 현재 페이지 번호


//지역코드, 시군구코드 목록을 조회하는 기능입니다.
//지역기반 관광정보 및 키워드 검색을 통해 지역별로 목록을 보여줄 경우, 지역코드를 이용하여 지역명을 매칭하기 위한 기능입니다.
exports.areaCode = (req,res,next) => {

	var queryParams='';

	queryParams += '&' + encodeURIComponent('pageNo') + '=' + encodeURIComponent(req.body.pageNo); /* 	현재 페이지번호  */
	queryParams += '&' + encodeURIComponent('numOfRows') + '=' + encodeURIComponent(req.body.numOfRows); /* 	한 페이지 결과 수  */


	if(typeof req.body.areaCode != "undefined" && req.body.areaCode != null ){
		queryParams += '&' + encodeURIComponent('areaCode') + '=' + encodeURIComponent(req.body.areaCode); //코드
	}

	console.log(queryParams);
	callAPI('koreaTour',queryParams , "/areaCode" , (result) => {
			res.json(result); 
	});
}








// mongo db connect request


exports.allSpotCount = (req,res,next) => {
    
    let $Camp = Camp.countDocuments();
    
    if(!isEmpty(req.body.doNm)){
        $Camp.where('doNm').regex('.*'+ req.body.doNm +'.*');
    }
    if(!isEmpty(req.body.sigunguNm)){
        $Camp.where('sigunguNm').regex('.*'+ req.body.sigunguNm +'.*');
    }


    $Camp.exec()
        .then( count => {
            console.log(count);
            res.json( {status: "S" ,  message : "success" , data : { totalRow : count } });
        })
        .catch(err => {
            console.log(err);
            res.json({status: "E" ,  message : err});
        });
        

    
}



//infoList 를 불러 오기 (paging 정보를 넣으면 paging 정보대로 가져옴)

exports.campInfoList = (req,res,next) => {
    console.log(req.body);


    var skip =  (Number(req.body.pageNumber)-1 ) * Number(req.body.showingContentNum); 
    var limit = Number(req.body.showingContentNum);
    
    let $Camp = Camp.find();
    
    if(!isEmpty(req.body.doNm)){
        $Camp.where('doNm').regex('.*'+ req.body.doNm +'.*');
    }
    if(!isEmpty(req.body.sigunguNm)){
        $Camp.where('sigunguNm').regex('.*'+ req.body.sigunguNm +'.*');
    }
    
    //2021-06-21 도 - 시군구 정보를 다이나믹 where 조건으로 받을 수 있도록 변경
    //doNm 도 정보
    //sigunguNm 시군구 정보



    $Camp.limit(limit).skip(skip).sort({createdtime : 1}).exec()
        .then( result => {
            res.json( {status: "S" ,  message : "success" , data : result });
        })
        .catch(err => {
            console.log(err);
            res.json({status: "E" ,  message : err});
        });
        

    
}



//디테일페이지이동시 contentId에 따른 디테일정보 가져옴
exports.getDetailCampingInfo = (req,res,next) => {
    console.log(req.body);
    let contentId = req.body.contentId;

    

    Camp.find({contentId:contentId}).exec()
        .then( result => {
            res.json( {status: "S" ,  message : "success" , data : result });
        })
        .catch(err => {
            console.log(err);
            res.json({status: "E" ,  message : err});
        });
}


//디테일페이지 이동
exports.getDetailInfo = (req,res,next) => {
    res.render('detail');
}

//미구현
exports.chngLocation = (req,res,next) => {
    Camp.find().exec()
        .then(result => {
            
            for(let i=0; i<1; i++){

                // let geo = new Camp({
                //     location: {
                //      type: "Point",
                //      coordinates: [result[i].mapY, result[i].mapX]
                //     }
                //    });
                //    geo.


                // console.log(result[i]._id);
                // Camp.findAndModify({query: { _id: result[i]._id }
                // , update : { 
                //     $set: {location.coordinates: [result[i].mapY,result[i].mapX]}
                // }}).exec()
                // .then(()=>{
                //     console.log("done  => " + i);
                // })
                // .catch(err => {
                //     console.log(err);
                //     res.json({status: "E" ,  message : err});
                // });
            }

        })
        .catch(err => {
            console.log(err);
            res.json({status: "E" ,  message : err});
        });
}


// 넘어온 값이 빈값인지 체크합니다. // !value 하면 생기는 논리적 오류를 제거하기 위해 // 명시적으로 value == 사용 // [], {} 도 빈값으로 처리 
let isEmpty = function(value){ 
    if( value == "" || value == null || value == undefined || ( value != null && typeof value == "object" && !Object.keys(value).length ) ){
        return true 
        }else{ 
            return false 
        } 
    };

