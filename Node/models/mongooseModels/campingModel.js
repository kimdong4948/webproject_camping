'use strict';

const mongoose = require('mongoose');

//서울 시간 format 으로 변경
const moment = require('moment');
moment.tz.setDefault("Asia/Seoul");
let date = moment().format('YYYY-MM-DD HH:mm:ss');

const pointSchema = new mongoose.Schema({
    type: {
        type: String,
        enum: ['Point'],
        required: true
    },
    coordinates: {
        type: [Number],
        required: true
    }
    });

const CampingBaseInfoSchema = new mongoose.Schema({
    addr1: { type: String },
    allar: { type: String },
    animalCmgCl: { type: String },
    autoSiteCo: { type: String },
    brazierCl: { type: String },
    caravAcmpnyAt: { type: String },
    caravSiteCo: { type: String },
    clturEventAt: { type: String },
    contentId: { type: String },
    createdtime: { type: String },
    doNm: { type: String },
    exprnProgrmAt: { type: String },
    extshrCo: { type: String },
    facltNm: { type: String },
    fireSensorCo: { type: String },
    frprvtSandCo: { type: String },
    frprvtWrppCo: { type: String },
    glampSiteCo: { type: String },
    gnrlSiteCo:{ type: String },
    induty: { type: String },
    indvdlCaravSiteCo: { type: String },
    insrncAt: { type: String },
    manageNmpr: { type: String },
    manageSttus: { type: String },
    mangeDivNm: { type: String },
    mapX: { type: String },
    mapY: { type: String },
    modifiedtime: { type: String },
    operDeCl: { type: String },
    operPdCl: { type: String },
    prmisnDe: { type: String },
    sigunguNm: { type: String },
    siteBottomCl1: { type: String },
    siteBottomCl2: { type: String },
    siteBottomCl3: { type: String },
    siteBottomCl4: { type: String },
    siteBottomCl5: { type: String },
    siteMg1Co: { type: String },
    siteMg1Vrticl: { type: String },
    siteMg1Width: { type: String },
    siteMg2Co: { type: String },
    siteMg2Vrticl: { type: String },
    siteMg2Width: { type: String },
    siteMg3Co: { type: String },
    siteMg3Vrticl: { type: String },
    siteMg3Width: { type: String },
    sitedStnc: { type: String },
    swrmCo: { type: String },
    tel: { type: String },
    toiletCo: { type: String },
    trlerAcmpnyAt: { type: String },
    wtrplCo: { type: String },
    zipcode: { type: String },
    location: {
        type: pointSchema,
            required: true
    }
}, { collection : 'CampingBaseInfo', // 컬렉션명 지정 
    versionKey : false , // "__v" 필드 생성X 
    strict : false
});

//CampingBaseInfoSchema.index({ location: "2dsphere" });


module.exports = mongoose.model('CampingBaseInfo', CampingBaseInfoSchema);