'use strict';


const mongoose = require('mongoose');
const crypto = require('crypto');

//서울 시간 format 으로 변경
const moment = require('moment-timezone');
moment.tz.setDefault("Asia/Seoul");
let date = moment().format('YYYY-MM-DD HH:mm:ss');



const UserSchema = new mongoose.Schema({
  id: { type: String, required: true, unique: true, 'default': '' },
  hashed_pwd: { type: String, required: true, 'default': '' },
  salt: { type: String, required: true },
  name: { type: String, index: 'hashed', 'default': '' },

  kakao: {
    //unique: true
  },
  provider: {
    type: String
    , 'default': 'local'
  },
  email: {
    type: String,
    unique: true
  },
  created_at: { type: Date, index: { unique: false }, 'default': date },
  updated_at: { type: Date, index: { unique: false }, 'default': date },
  roles: [{
    type: String,
  }]
}, {
    versionKey: false
});


// password를 virtual 메소드로 정의 : MongoDB에 저장되지 않는 가상 속성임. 
// 특정 속성을 지정하고 set, get 메소드를 정의함
UserSchema
  .virtual('password')
  .set(function (password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.hashed_pwd = this.encryptPassword(password);
    console.log('virtual password의 set 호출됨 : ' + this.hashed_pwd);
  })
  .get(function () {
    console.log('virtual password의 get 호출됨.');
    return this._password;
  });


  UserSchema
  .virtual('kakao_id')
  .set(function (kakao_id) {
    this._kakao_id = kakao_id;
    //this.salt = this.makeSalt();
    this.id = this.encryptID(kakao_id);
    console.log('virtual id의 set 호출됨 : ' + this.id);
  })
  .get(function () {
    console.log('virtual id의 get 호출됨.');
    return this._kakao_id;
  });  

// 스키마에 모델 인스턴스에서 사용할 수 있는 메소드 추가
// 비밀번호 암호화 메소드
UserSchema.method('encryptPassword', function (plainText, inSalt) {
  if(typeof plainText == 'number'){
    plainText = plainText.toString();
  }
  
  if (inSalt) {
    return crypto.createHmac('sha1', inSalt).update(plainText).digest('hex');
  } else {
    return crypto.createHmac('sha1', this.salt).update(plainText).digest('hex');
  }
});

//id encrypt
UserSchema.method('encryptID', function (plainText) {

  if(typeof plainText == 'number'){
    plainText = plainText.toString();
  }

  return crypto.createHash('sha1').update(plainText).digest('hex');
});


// salt 값 만들기 메소드
UserSchema.method('makeSalt', function () {
  return Math.round((new Date().valueOf() * Math.random())) + '';
});

// 인증 메소드 - 입력된 비밀번호와 비교 (true/false 리턴)
UserSchema.method('authenticate', function (plainText, inSalt, hashed_password) {
  if (inSalt) {
    console.log('authenticate 호출됨 : %s -> %s : %s', plainText, this.encryptPassword(plainText, inSalt), hashed_password);
    return this.encryptPassword(plainText, inSalt) === hashed_password;
  } else {
    console.log('authenticate 호출됨 : %s -> %s : %s', plainText, this.encryptPassword(plainText), this.hashed_password);
    return this.encryptPassword(plainText) === this.hashed_password;
  }
});

// 값이 유효한지 확인하는 함수 정의
var validatePresenceOf = function (value) {
  return value && value.length;
};

// 저장 시의 트리거 함수 정의 (password 필드가 유효하지 않으면 에러 발생)
UserSchema.pre('save', function (next) {
  if (!this.isNew) return next();

  if (!validatePresenceOf(this.hashed_pwd)) {
    next(new Error('유효하지 않은 password 필드입니다.'));
  } else {
    next();
  }
});

// 필수 속성에 대한 유효성 확인 (길이값 체크)
UserSchema.path('id').validate(function (id) {
  return id.length;
}, 'id 칼럼의 값이 없습니다.');

UserSchema.path('name').validate(function (name) {
  return name.length;
}, 'name 칼럼의 값이 없습니다.');

UserSchema.path('hashed_pwd').validate(function (hashed_pwd) {
  return hashed_pwd.length;
}, 'hashed_password 칼럼의 값이 없습니다.');


// 스키마에 static으로 findById 메소드 추가
UserSchema.static('findById', function (id, callback) {
  return this.find({ id: id }, callback);
});

// 스키마에 static으로 findAll 메소드 추가
UserSchema.static('findAll', function (callback) {
  return this.find({}, callback);
});


module.exports = mongoose.model('User', UserSchema);