'use strict';

const mongoose = require('mongoose');
const crypto = require('crypto');
const autoIncrement = require('mongoose-auto-increment');

var moment = require('moment');
moment.tz.setDefault("Asia/Seoul");

var date = moment().format('YYYY-MM-DD HH:mm:ss');

//seq 사용하기위한 init
autoIncrement.initialize(mongoose.connection);


const CommentSchema = new mongoose.Schema({
  comment_seq:  { type: Number, required: true, unique: true, required: true },
  content_id: { type: Number, required: true },
  rating: { type: Number, required: true },
  user_id: { type: mongoose.Schema.Types.ObjectId, required: true  , ref: 'User'},
  user_profile_url:{type: String , 'default': ''},
  comment:{ type: String, required: true },
  created_dt: { type: String, index: { unique: false }, 'default': date},
  updated_dt: { type: String, index: { unique: false }, 'default': date },

  //join user collection
  //userInfo : [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]

}, {
    versionKey: false
});

CommentSchema.plugin(autoIncrement.plugin,{ 
  model : 'Comment'
  , field : 'comment_seq'
  , startAt : 1  //시작 
  , increment : 1 // 증가 
});





module.exports = mongoose.model('Comment', CommentSchema);