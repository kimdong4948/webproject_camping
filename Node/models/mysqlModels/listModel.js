'use strict';



module.exports = (sequelize, DataTypes) => {

    const User = sequelize.define('user', {

        UID: {
            type: DataTypes.INTEGER
            ,allowNull: false
        },
        USER_ID: {
            type: DataTypes.STRING(300) 
            ,allowNull: false
        },
        USER_NM: {
            type: DataTypes.STRING(300) 
            ,allowNull: false
        },
        USER_PASSWORD: {
            type: DataTypes.STRING(4000)
            ,allowNull: false
        }
    
        }
        ,{
            //equelize 는 데이터 생성시와 수정시 시간을 기록하는 createdAt 컬럼과 updatedAt 컬럼을 기본적으로 생성하고 사용한다.
            timestamps: false,
            freezeTableName: true,
            //sequelize 는 테이블 이름을 기본적으로 복수형으로 생성한다.
            tableName : "user_info"
        });
    
        //sequelize는 1개의 primary key를 필요로 한다. primary key가 없는 경우 id 컬럼을 암시적으로 생성해서 primary key로 지정한다.
        User.removeAttribute('id');

        return User;

};
//기존 테이블 사용시



