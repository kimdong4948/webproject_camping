const Sequelize = require('sequelize');

var db_info = {
    host     : process.env.host,    // 호스트 주소
    user     : process.env.dbusername,           // mysql user
    password : process.env.password,       // mysql password
    database : process.env.database,
    dialect : process.env.dialect        // mysql 데이터베이스
};


// Option 1: 파라미터로 나눠서 전달하기
const sequelize = new Sequelize(db_info.database, db_info.user, db_info.password, {
    host: db_info.hostname,
    //port:  3306  //기본 포트가 아닐경우
    //커넥션 풀 생성
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },

    dialect: db_info.dialect /* 'mysql' | 'mariadb' | 'postgres' | 'mssql' 중에 하나 선택*/
});



module.exports.connect =  () =>  {
    
    sequelize
    .authenticate()
    .then(() => { 
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });


},



module.exports.close = () =>  {

    sequelize.close();

}
