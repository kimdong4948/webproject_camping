const mongoose = require('mongoose');
const util = require('util');
const debug = require('debug')('express-mongoose-es6-rest-api:index');

const config = require('../config/config');

const uri = "mongodb+srv://" + process.env.mongoHost + ":" + process.env.mongoPassword + "@cluster1.dmasx.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
// connect to mongo db

mongoose.connect(uri, { 
  keepAlive: 1,
  useCreateIndex: true,
  useNewUrlParser: true, 
  useUnifiedTopology: true
});

mongoose.connection.on('error', () => {
  throw new Error(`unable to connect to database: ${mongoUri}`);
});

// print mongoose logs in dev env
if (config.MONGOOSE_DEBUG) {
  mongoose.set('debug', (collectionName, method, query, doc) => {
    debug(`${collectionName}.${method}`, util.inspect(query, false, 20), doc);
  });
}
