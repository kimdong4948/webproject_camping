const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy; //LOCAL 전략
const KakaoStrategy = require('passport-kakao').Strategy; // KAKAO 전략
const Users = require('../../models/mongooseModels/userModel');

const User = require('../../models/User');

var crypto = require('crypto');


  passport.serializeUser(function(user, done) {  //정보 암호화
    var serial = {};
    serial["provider"] = user.provider;
    if(user.provider == 'kakao' ){  //카카오 정보 받아오도록 함
      serial["id"] = user.kakao.id;
    }else{ //LOCAL LOGIN 정보 받아오도록 함
      serial["id"] = user.id;
    }

    done(null,serial);
 
  });
  
  passport.deserializeUser(function(user, done) {     
    //console.log("deserial  ////////////  " + user.provider);
    if(user.provider == 'kakao'){ //카카오 정보 받아오도록 함
      Users.findOne({
        'kakao.id': user.id
      })
      .then( user => {
        
        //delete user[id];
        //user.splice(2, 1);
        user = user.toObject();

        removeUselessInfo(user);
        return done(null, user);
      })
      .catch( err => {
        return done(null, false, { error: 'Authorizaion Fail Please try again.' , data : err});
      })
    }else{ //LOCAL LOGIN 정보 받아오도록 함


      //기존 예슬씨 USER MODEL

      User.findOne({ _id: user.id })
      .then( user => {
        
        user = user.toObject();

        removeUselessInfo(user);
        return done(null, user);
      })
      .catch( err => {
        return done(null, false, { error: 'Authorizaion Fail Please try again.' , data : err});
      })

    }

    
  });


  const localLogin = new LocalStrategy({
    usernameField : 'username',
    passwordField : 'password',
    session: true,
    passReqToCallback : true
  },
  function(req, username, password, done) {
    User.findOne({username:username})
      .select({password:1})
      .exec(function(err, user) {
        if (err) return done(err);

        if (user && user.authenticate(password)){
          console.log(user);
          return done(null, user);
        }
        else {
          req.flash('username', username);
          req.flash('errors', {login:'The username or password is incorrect.'});
          return done(null, false);
        }
      });
  });


  const kakaoLogin = new KakaoStrategy(
    {
      clientID: process.env.kakaoClientID,
      callbackURL: process.env.kakaoCallbackURL,
    },
    function(accessToken, refreshToken, profile, done) {
      
      //console.log(profile);
      //console.log(accessToken);
      //console.log(refreshToken);
      //console.log(profile);

      Users.findOne(
        {
          'kakao.id': profile.id,
        },
        function(err, user) {
          if (err) {
            return done(err)
          }
          
          if (!user) {

            
            user = new Users({
              kakao_id: profile.id,
              password : profile.id,
              name:profile.username,
              //roles: ['authenticated'],
              provider: 'kakao',
              kakao: profile._json,
              email: profile._json.kakao_account.email
            })

            user.save(function(err) {
              if (err) {
                console.log(err)
              }

              user = user.toObject();
              removeUselessInfo(user);

              return done(err, user)
            })
          } else {


            user = user.toObject();
            removeUselessInfo(user);

  
            return done(err, user)
          }
        }
      )
    }
  );


  //passport.use(jwtLogin);
  passport.use(kakaoLogin);
  passport.use(localLogin);


  var removeUselessInfo = (obj) => {

    delete obj["hashed_pwd"];
    delete obj["salt"];

  };


  module.exports = passport;
